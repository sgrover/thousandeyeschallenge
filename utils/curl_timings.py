from __future__ import division
import subprocess
import json
from collections import defaultdict
import multiprocessing
import time
import re
from random import randint
import os
#import numpy as np
from fetch_site_info import load_sites
import CONST


def fetch_url(rank, url):
    """fetch url using curl
    --connect-timeout 5.0s optional, max timeout -m 10.0s, follow redirects using -L flag
    result is ( dict {url_effective, response_code, time_namelookup, time_connect, time_appconnect,
    time_pretransfer, time_redirect, time_starttransfer, time_total}, exception )
    variable explanations: https://ec.haxx.se/usingcurl-verbose.html
    """
    try:
        #remove -L redirects as it messes up all timings esp ssl based due to multiple redirs
        p = subprocess.Popen(['curl', '--connect-timeout', '5.0', '-m', '10.0', '-o', '/dev/null',
                              '-w', '@curl_time_format.txt', '-s', url], stdout=subprocess.PIPE,
                             universal_newlines=True)       #universal newlines no more byte format so no decoding
        out, err = p.communicate()
        #out = re.sub(r'(\d+),(\d+)', r'\1.\2', out)     # in case , used instead of decimal
        result = json.loads(out)
        result['rank'] = rank                           # adding rank later to make future merges easier
        result['timestamp'] = time.time()               # add timestamp for plotting

        if (result['response_code'] != '200') and (result['response_code'] != 200):
            print("Rank %s site %r fetched with response code %r in %ss"
                  % (rank, url, result['response_code'], result['time_total']))

    except Exception as e:
        print("Error fetching %r: Exception %s" % (url, e))
        result = None
    return result


def parallel_requests(list_of_websites=CONST.list_of_websites,
                      nwebsites=CONST.nwebsites,
                      nthreads=CONST.nthreads,
                      count=CONST.count):
    """
    parallel curl requests distributed among limited threads with 10-30s random time between subsequent loops
    :param list_of_websites: path to top-1m.csv
    :param nwebsites: 500
    :param nthreads: 25
    :param count: 100
    :return:
    """
    sites = load_sites(list_of_websites, nwebsites)
    urls = {rank:'https://www.' + site + '/' for rank, site in sites.items()}
    rank_url_tuples = list(urls.items())

    data = defaultdict(list)  # save result as json and load in pandas for averaging and analysis

    for i in range(count):

        print("\nLoop: %s, Start time: %s" % (i, time.time()))

        pool = multiprocessing.Pool(processes=nthreads)
        # starmap passes multiple args
        pool_outputs = pool.starmap(fetch_url, rank_url_tuples)  # pool_output is a list of results

        pool.close()
        pool.join()

        time.sleep(randint(10, 30))  # add random sleep time as certain google servers become annoying

        for res in pool_outputs:
            if res is not None:
                [data[key].append(res[key]) for key in res.keys()]

    return data


def save_curl_timings_data(data):
    savefile = CONST.curl_timing_data_json
    with open(savefile, 'w') as outfile:
        json.dump(data, outfile)
    print("Save curl timing data to %s" % (savefile))
    return


def load_urls(websites, nwebsites=500):
    """get top 500 of alexa websites csv <RANK, SITE> and append with 'https://www.' for curl request"""
    urls = {}
    from itertools import islice
    with open(websites) as f:
        for line in islice(f, nwebsites):
            rank, site = line.strip().split(',')
            url = 'https://www.' + site + '/'
            urls[int(rank)] = url
    return urls


def main():

    list_of_websites = CONST.list_of_websites
    nwebsites = CONST.nwebsites
    count = CONST.count
    nthreads = CONST.nthreads
    data = parallel_requests(list_of_websites, nwebsites, nthreads, count)

    # check for output directory to save files
    outdir = 'output/'
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    save_curl_timings_data(data)

    return


if __name__ == '__main__':

    main()
